//
//  Admin.swift
//  FamilyHealthCare
//
//  Created by Mohamed Habib Jaberi on 03/04/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//

import Foundation
class Admin : User{
  
  func createFamily(_ name : String) -> Family{
    let family = Family.init(name, user: self)
    return family
  }
  func addMember(_ family: Family , member :User){
    family.members.append(member)
  }
  func removeMember(_ family: Family ,_ name : String){
    var index = 0
    while family.members[index].userInfo["name"] as! String != name && index < family.members.count {
      index = index+1
      
    }
    family.members.remove(at: index)
  }
}
