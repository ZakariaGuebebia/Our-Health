//
//  Document.swift
//  FamilyHealthCare
//
//  Created by Mohamed Habib Jaberi on 03/04/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//

import Foundation
class Document : NSObject{
  
  var documentName: String
  
  init(_ name : String ) {
    
    self.documentName = name
  }
  
  
  override init() {
    
    documentName = ""
  }
}
