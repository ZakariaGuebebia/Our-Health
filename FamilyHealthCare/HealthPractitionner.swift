//
//  HealthPractitionner.swift
//  FamilyHealthCare
//
//  Created by Mohamed Habib Jaberi on 03/04/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//

import Foundation
class HealthPractitionner : NSObject{
  
  var practitionnerName: String
  var practitionnerAdress: String
  
  
  
  init(_ name : String , _ adress: String) {
    self.practitionnerName = name
    self.practitionnerAdress = adress
  }
  
  override init() {
    practitionnerName = ""
     practitionnerAdress = ""
  }
}


