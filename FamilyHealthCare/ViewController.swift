//
//  ViewController.swift
//  FamilyHealthCare
//
//  Created by Mohamed Habib Jaberi on 28/03/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase


class ViewController: UIViewController {
  @IBOutlet weak var emailField: UITextField!
  @IBOutlet weak var passwordField: UITextField!
  
  var ref : DatabaseReference?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    ref = Database.database().reference()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  @IBAction func loginButton(_ sender: Any) {
    
    Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!) { (user, error) in
      if error != nil {
        print("email login error", error as Any)
      }
      else {
        print("user signed in via email")
      
      }
    }
    
    
  }
  
  @IBAction func registerButton(_ sender: Any) {
    Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!) { (user, error) in
      if error != nil {
        print("email login error", error as Any)
      }
      else {
        print("user created via email")
       
      }
    }
    
   
  }
  
  @IBAction func resetPasswordButton(_ sender: Any) {
  }
}

