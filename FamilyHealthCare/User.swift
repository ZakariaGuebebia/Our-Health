//
//  User.swift
//  FamilyHealthCare
//
//  Created by Mohamed Habib Jaberi on 03/04/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//
import Foundation
class User : NSObject{
  
  var userId: String
  var userInfo : [String:Any]
  var documents : [Document]
  var recievedDocuments : [Document]
  
  func getDocumentByName(_ name : String , _ documents: [Document]) -> Document{
    var i = 0
    var document = documents[i]
    while name != document.documentName && i < documents.count{
      i = i+1
      document = documents[i]
    }
    return document
  }
  
  func sendDocument(_ document : Document, _ to : User){
    to.recievedDocuments.append(document)
    
  }
  
  init(_ userid : String , _ userinfo: [String:Any], _ documents: [Document], _ recievedDocuments: [Document]  ) {
    self.userId = userid
    self.userInfo = userinfo
    self.documents = documents
    self.recievedDocuments = recievedDocuments
  }
  
  override init() {
    userId = ""
    userInfo = [String:Any]()
    self.documents = [Document]()
    self.recievedDocuments = [Document]()
  }
}
