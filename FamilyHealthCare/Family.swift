//
//  Family.swift
//  FamilyHealthCare
//
//  Created by Mohamed Habib Jaberi on 03/04/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//

import Foundation
class Family : NSObject{
  
  var familyName : String
  var members : [User]
  
  init(_ name: String, user : User) {
    self.familyName = name
    self.members = [User]()
    self.members.append(user)
  }
  
  override init() {
    familyName = ""
    members = [User]()
    
  }
}


