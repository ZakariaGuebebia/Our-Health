//
//  SessionManager.swift
//  FamilyHealthCare
//
//  Created by Mohamed Habib Jaberi on 03/04/2018.
//  Copyright © 2018 Mohamed Habib Jaberi. All rights reserved.
//

import Foundation
class SessionManager {
  
  static let shared = SessionManager()
  var currentUser : User
  private init(){
    currentUser = User.init()
  }
}

